/*
 Name:		timings.ino
 Created:	11/26/2016 10:02:47 AM
 Author:	omudhir
*/

#include "cbuffer.h"

struct Point
{
  uint16_t x = 0;
  uint16_t y = 0;
/*  Point& operator+=(const Point& rhs)
  {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }
  Point operator/(float rhs)
  {
    Point p;
    p.x = x/rhs;
    p.y = y/rhs;
    return p;
  }*/
};

#define N 16
struct Sensor
{
  uint64_t t0 = 0;
  uint16_t high = 0; // pause duration
  uint16_t low = 0;  // pulse duration
  Point p[32];
  Point tmp, cur;
  Point avg;
  int index = 0;
  char stage = 0;
  char value = HIGH;
  bool frame_ready = false;
  char pin = 0;
  Point average()
  {
    uint32_t sumx;
    uint32_t sumy;
    for (int i = 0; i < 32; i++)
    {
      sumx += p[i].x;
      sumy += p[i].y;
    }
    avg.x = sumx * (1.f/32.f);
    avg.y = sumy * (1.f/32.f);
    return avg;
  }
  void toggle()
  {
    uint64_t t = micros();
    if (value == HIGH)
    {
      high = t - t0;
      if (high > 100)
        value = LOW;
    }
    else
    {
      value = HIGH;
      low = t - t0;
      if (stage == 0 && high > 8000 && low > 100)
      {
        stage++;
      }
      else if (stage == 1 && high > 8000 && low < 100)
      {
          stage++;
      }
      else if (stage == 2)
      {
          tmp.x = high;
          stage++;
      }
      else if (stage < 6)
      {
          if (low > 100)
          {
            tmp.y = high;
            cur = tmp;
            stage = 0;
            frame_ready = true;
          }
          else
          {
            stage++;
          }
      }
      else
      {
        stage = 0;
      }
    }
    t0 = t;
  }
};
Sensor s[2];

void setup()
{
    Serial.begin(115200);
    s[0].pin = 4;
    s[1].pin = 12;
    auto t0 = micros();
    for (auto& ss : s)
    {
      ss.t0 = t0;
      pinMode(ss.pin, INPUT);
    }
}

void loop() 
{
    for (int i = 0; i < 4; i++)
    {
      bool is_ready = false;
      while(!is_ready)
      {
        is_ready = true;
        for (auto& ss : s)
        {
          int v = digitalRead(ss.pin);
          if (ss.value != v)
              ss.toggle();
          is_ready &= ss.frame_ready;
        }
      }
    }

    Serial.print("(");
    for (auto& ss : s)
    {
      static char str[32];
      ss.frame_ready = false;
      ss.stage = 0;
      sprintf(str, "%hu %hu;", ss.cur.x, ss.cur.y);
      Serial.print(str);
    }
    Serial.print(")");
}
