#pragma once

/* Usage example
 int main(int argc, const char * argv[])
 {
 cbuffer<int> a(5);
 a.add(1);
 a.add(2);
 a.add(3);
 a.add(4);
 
 std::printf("avg %f\n", a.average<float>());
 for (auto& i : a)
 {
 i = 0;
 std::printf("%d\n", i);
 }
 return 0;
 }
 */

template<typename T, int N> struct cbuffer
{
    T m_vec[N];
    int m_capacity;
    int m_count;
    int m_index;
    cbuffer()
    {
        m_capacity = N;
        m_index = 0;
        m_count = 0;
    }
    T& head()
    {
        return m_index == 0 ? m_vec[m_count - 1] : m_vec[m_index - 1];
    }
    void add(const T& v)
    {
        m_vec[m_index] = v;
        m_index = (m_index + 1) % m_capacity;
        m_count = m_count < m_capacity ? m_count + 1 : m_count;
    }
    int count() const
    {
        return m_count;
    }
    template<typename T2 = T> T2 average() const
    {
        T2 tot{};
        if(m_count == 0)
            return tot;
        for (int i = 0; i < m_count; i++)
            tot += m_vec[i];
        return tot / (float)m_count;
    }
};

