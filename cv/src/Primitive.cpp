#include "stdafx.h"
#include "Primitive.h"
#include "Shader.h"
#include <math.h>
#include <cmath>

BaseElement::BaseElement()
{
    ibuf = 0;
    vbuf = 0;
    i_count = 0;
    p_offset = 0;
    t_offset = 0;
    n_offset = 0;
    v_stride = 0;
    color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

void BaseElement::draw(GLenum type) const
{
//     GL_CHECK( glActiveTexture(TEX_DIFFUSE) );
//     GL_CHECK( glClientActiveTexture(TEX_DIFFUSE) );
//     GL_CHECK( glBindTexture(GL_TEXTURE_2D, material.diffuse_tex) );
    
    //glActiveTexture(TEX_NORMAL);
    //glBindTexture(GL_TEXTURE_2D, material.normal_tex);
    //
    //glActiveTexture(TEX_SPECULAR);
    //glBindTexture(GL_TEXTURE_2D, material.specular_tex);
    //
    //glActiveTexture(TEX_SHADOW);
    //glBindTexture(GL_TEXTURE_2D, material.shadow_tex);
    
    GL_CHECK( glBindBuffer(GL_ARRAY_BUFFER, vbuf) );
    GL_CHECK( glVertexPointer(3, GL_FLOAT, v_stride, (void*)p_offset) );
    GL_CHECK( glNormalPointer(GL_FLOAT, v_stride, (void*)n_offset) );
    GL_CHECK( glTexCoordPointer(2, GL_FLOAT, v_stride, (void*)t_offset) );
    
    GL_CHECK( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf) );
    GL_CHECK( glDrawElements(type, i_count, GL_UNSIGNED_INT, 0) );

    GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

void Plane::create(GLfloat w, GLfloat h)
{
    glm::vec3 pos[4], nor[4], tan[4];
    glm::vec2 tex[4];
    GLuint idx[6];
    w *= 0.5f;
    h *= 0.5f;
    GLfloat z = 0.0f;
    pos[0] = glm::vec3(-w, -h, z);
    pos[1] = glm::vec3(w, -h, z);
    pos[2] = glm::vec3(w, h, z);
    pos[3] = glm::vec3(-w, h, z);
    tex[0] = glm::vec2(0, 0);
    tex[1] = glm::vec2(1, 0);
    tex[2] = glm::vec2(1, 1);
    tex[3] = glm::vec2(0, 1);
    nor[0] = glm::vec3(0, 0, 1);
    nor[1] = glm::vec3(0, 0, 1);
    nor[2] = glm::vec3(0, 0, 1);
    nor[3] = glm::vec3(0, 0, 1);
    tan[0] = glm::vec3(0, 0, 1);
    tan[1] = glm::vec3(0, 0, 1);
    tan[2] = glm::vec3(0, 0, 1);
    tan[3] = glm::vec3(0, 0, 1);

    GLuint indexes[] = { 0, 1, 2, 2, 3, 0 };
    memcpy(idx, indexes, sizeof(idx));
    
    long i_size = sizeof(idx);
    long p_size = sizeof(pos);
    long n_size = sizeof(nor);
    long tg_size = sizeof(tan);
    long t_size = sizeof(tex);

    GL_CHECK( glGenBuffers(1, &ibuf) );
    GL_CHECK( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf) );
    GL_CHECK( glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, &idx[0], GL_STATIC_DRAW) );
    
    i_count = 6;
    p_offset = 0;
    n_offset = p_size;
    tg_offset = p_size + n_size;
    t_offset = p_size + n_size + tg_size;
    GL_CHECK( glGenBuffers(1, &vbuf) );
    GL_CHECK( glBindBuffer(GL_ARRAY_BUFFER, vbuf) );
    GL_CHECK( glBufferData(GL_ARRAY_BUFFER, p_size+n_size+t_size+tg_size, NULL, GL_STATIC_DRAW) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, &pos[0]) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, &nor[0]) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, tg_offset, tg_size, &tan[0]) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, &tex[0]) );
}

void Cube::create(GLfloat x, GLfloat y, GLfloat z)
{
    glm::vec3 pos[] {
        // Front   x   y   z
        glm::vec3(-x, -y, -z),
        glm::vec3(-x,  y, -z),
        glm::vec3( x,  y, -z),
        glm::vec3( x, -y, -z),
        // Back    x   y   z
        glm::vec3( x, -y,  z),
        glm::vec3( x,  y,  z),
        glm::vec3(-x,  y,  z),
        glm::vec3(-x, -y,  z),
//         // Left    x   y   z
//         glm::vec3(-x, -y,  z),
//         glm::vec3(-x,  y,  z),
//         glm::vec3(-x,  y, -z),
//         glm::vec3(-x, -y, -z),
//         // Right   x   y   z
//         glm::vec3(+x, -y, -z),
//         glm::vec3(+x,  y, -z),
//         glm::vec3(+x,  y, +z),
//         glm::vec3(+x, -y, +z),
//         // Top     x   y   z
//         glm::vec3(-x,  y, -z),
//         glm::vec3(-x,  y,  z),
//         glm::vec3( x,  y,  z),
//         glm::vec3( x,  y, -z),
//         // Bottom  x   y   z
//         glm::vec3(-x, -y,  z),
//         glm::vec3(-x, -y, -z),
//         glm::vec3( x, -y, -z),
//         glm::vec3( x, -y,  z),
    };
    std::copy(pos, pos + 8, points);
    glm::vec2 tex[] {
        // Front
        glm::vec2(0.25f, 0.50f),
        glm::vec2(0.25f, 0.75f),
        glm::vec2(0.50f, 0.75f),
        glm::vec2(0.50f, 0.50f),
        // Back
        glm::vec2(0.25f, 0.00f),
        glm::vec2(0.25f, 0.25f),
        glm::vec2(0.50f, 0.25f),
        glm::vec2(0.50f, 0.00f),
//         // Left
//         glm::vec2(0.00f, 0.00f),
//         glm::vec2(0.00f, 0.25f),
//         glm::vec2(0.25f, 0.25f),
//         glm::vec2(0.25f, 0.00f),
//         // Right
//         glm::vec2(0.50f, 0.00f),
//         glm::vec2(0.50f, 0.25f),
//         glm::vec2(0.75f, 0.25f),
//         glm::vec2(0.75f, 0.00f),
//         // Top
//         glm::vec2(0.25f, 0.25f),
//         glm::vec2(0.25f, 0.50f),
//         glm::vec2(0.50f, 0.50f),
//         glm::vec2(0.50f, 0.25f),
//         // Bottom
//         glm::vec2(0.25f, 0.75f),
//         glm::vec2(0.25f, 1.00f),
//         glm::vec2(0.50f, 1.00f),
//         glm::vec2(0.50f, 0.75f),
    };
    GLuint idx[] {
        0, 1, 2, 3,     // Front
        4, 5, 6, 7,     // Back
//         8, 9, 10, 11,   // Left
//         12, 13, 14, 15, // Right
//         16, 17, 18, 19, // Top
//         20, 21, 22, 23, // Bottom
    };

    long i_size = sizeof(idx);
    long p_size = sizeof(pos);
    long t_size = sizeof(tex);

    GL_CHECK( glGenBuffers(1, &ibuf) );
    GL_CHECK( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf) );
    GL_CHECK( glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, &idx[0], GL_STATIC_DRAW) );

    i_count = _countof(idx);
    p_offset = 0;
    t_offset = p_size;
    GL_CHECK( glGenBuffers(1, &vbuf) );
    GL_CHECK( glBindBuffer(GL_ARRAY_BUFFER, vbuf) );
    GL_CHECK( glBufferData(GL_ARRAY_BUFFER, p_size + t_size, NULL, GL_STATIC_DRAW) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, &pos[0]) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, &tex[0]) );
}

void Cube::create(GLfloat l, int divisions)
{
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texcoords;
    std::vector<GLuint> indices;

    struct plane_s
    {
        // origin
        glm::vec3 O;
        // tangent and binormal
        glm::vec3 X;
        glm::vec3 Y;
        plane_s(glm::vec3 O, glm::vec3 X, glm::vec3 Y) : O(O), X(X), Y(Y) {}
    };

    std::vector<plane_s> planes = {
        { glm::vec3{ 0, 0,-1 } * l * 0.5f, { 1, 0, 0 }, { 0, 1, 0 } }, // front
        { glm::vec3{ 1, 0, 0 } * l * 0.5f, { 0, 0, 1 }, { 0, 1, 0 } }, // right
        { glm::vec3{ 0, 0, 1 } * l * 0.5f, {-1, 0, 0 }, { 0, 1, 0 } }, // back
        { glm::vec3{-1, 0, 0 } * l * 0.5f, { 0, 0,-1 }, { 0, 1, 0 } }, // left
        { glm::vec3{ 0, 1, 0 } * l * 0.5f, { 1, 0, 0 }, { 0, 0, 1 } }, // top
        { glm::vec3{ 0,-1, 0 } * l * 0.5f, { 1, 0, 0 }, { 0, 0,-1 } }, // right
    };

    // allocate data
    int npoints = (divisions + 1) * (divisions + 1) * planes.size();
    vertices.resize(npoints * 3);
    normals.resize(npoints * 3);
    texcoords.resize(npoints * 2);
    indices.resize(divisions * divisions * 4 * planes.size());
    // pointers
    std::vector<GLfloat>::iterator v = vertices.begin();
    std::vector<GLfloat>::iterator n = normals.begin();
    std::vector<GLfloat>::iterator t = texcoords.begin();
    std::vector<GLuint>::iterator idx = indices.begin();
    for (int plane_id = 0; plane_id < (int)planes.size(); plane_id++)
    {
        auto plane = planes[plane_id];
        // directions
        glm::vec3 X = plane.X;
        glm::vec3 Y = plane.Y;
        //glm::vec3 Z = glm::cross(X, Y); // calculate the normal
        glm::vec3 Z(plane_id * 0.1f); // use a fake normal to pass the plane ID for picking
        // spacing
        auto dx = X / divisions * l;
        auto dy = Y / divisions * l;
        // offset
        auto ox = -dx * divisions * 0.5;
        auto oy = -dy * divisions * 0.5;
        for (int x = 0; x <= divisions; x++)
        {
            for (int y = 0; y <= divisions; y++)
            {
                auto P = (ox + dx * x) + (oy + dy * y) + plane.O;
                auto T = glm::vec2(x, y) / divisions;
                auto N = Z;
                *t++ = T.x;
                *t++ = T.y;
                *v++ = P.x;
                *v++ = P.y;
                *v++ = P.z;
                *n++ = N.x;
                *n++ = N.y;
                *n++ = N.z;
            }
        }
        // generate indices
        int idx_offset = (divisions + 1) * (divisions + 1) * plane_id;
        for (int x = 0; x < divisions; x++)
        {
            for (int y = 0; y < divisions; y++)
            {
                int p0 = x * (divisions + 1) + y;
                int p3 = (x + 1) * (divisions + 1) + y;
                int p1 = p0 + 1;
                int p2 = p3 + 1;
                *idx++ = p0 + idx_offset;
                *idx++ = p1 + idx_offset;
                *idx++ = p2 + idx_offset;
                *idx++ = p3 + idx_offset;
            }
        }
    }

    int i_size = (int)indices.size() * sizeof(unsigned int);
    int p_size = (int)vertices.size() * sizeof(float);
    int n_size = (int)normals.size() * sizeof(float);
    int t_size = (int)texcoords.size() * sizeof(float);

    GL_CHECK( glGenBuffers(1, &ibuf) );
    GL_CHECK( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf) );
    GL_CHECK( glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, indices.data(), GL_STATIC_DRAW) );

    i_count = (int)indices.size();
    p_offset = 0;
    n_offset = p_offset + p_size;
    t_offset = n_offset + n_size;
    int tot_size = t_offset + t_size;
    GL_CHECK( glGenBuffers(1, &vbuf) );
    GL_CHECK( glBindBuffer(GL_ARRAY_BUFFER, vbuf) );
    GL_CHECK( glBufferData(GL_ARRAY_BUFFER, tot_size, NULL, GL_STATIC_DRAW) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, vertices.data()) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, normals.data()) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, texcoords.data()) );
}

Material::Material()
{
    diffuse_tex = 0;
    normal_tex = 0;
    specular_tex = 0;
    shadow_tex = 0;
}

void Sphere::create(float radius, int rings, int sectors)
{
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texcoords;
    std::vector<GLuint> indices;

    float const R = 1.f / (float)(rings-1);
    float const S = 1.f / (float)(sectors-1);
    int r, s;
    
    vertices.resize(rings * sectors * 3);
    normals.resize(rings * sectors * 3);
    texcoords.resize(rings * sectors * 2);
    std::vector<GLfloat>::iterator v = vertices.begin();
    std::vector<GLfloat>::iterator n = normals.begin();
    std::vector<GLfloat>::iterator t = texcoords.begin();
    for(r = 0; r < rings; r++) for(s = 0; s < sectors; s++) {
        float const y = (float)sin( -M_PI_2 + M_PI * r * R );
        float const x = (float)cos(2*M_PI * s * S) * (float)sin( M_PI * r * R );
        float const z = (float)sin(2*M_PI * s * S) * (float)sin( M_PI * r * R );
        
        *t++ = s*S;
        *t++ = r*R;
        
        *v++ = x * radius;
        *v++ = y * radius;
        *v++ = z * radius;
        
        *n++ = x;
        *n++ = y;
        *n++ = z;
    }
    
    indices.resize(rings * sectors * 4);
    std::vector<GLuint>::iterator i = indices.begin();
    for(r = 0; r < rings-1; r++) for(s = 0; s < sectors-1; s++) {
        *i++ = r * sectors + s;
        *i++ = r * sectors + (s+1);
        *i++ = (r+1) * sectors + (s+1);
        *i++ = (r+1) * sectors + s;
    }
    
    int i_size = (int)indices.size() * sizeof(unsigned int);
    int p_size = (int)vertices.size() * sizeof(float);
    int n_size = (int)normals.size() * sizeof(float);
    int t_size = (int)texcoords.size() * sizeof(float);
    
    GL_CHECK( glGenBuffers(1, &ibuf) );
    GL_CHECK( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf) );
    GL_CHECK( glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, indices.data(), GL_STATIC_DRAW) );
    
    i_count = (int)indices.size();
    p_offset = 0;
    n_offset = p_offset + p_size;
    t_offset = n_offset + n_size;
    int tot_size = t_offset + t_size;
    GL_CHECK( glGenBuffers(1, &vbuf) );
    GL_CHECK( glBindBuffer(GL_ARRAY_BUFFER, vbuf) );
    GL_CHECK( glBufferData(GL_ARRAY_BUFFER, tot_size, NULL, GL_STATIC_DRAW) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, vertices.data()) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, normals.data()) );
    GL_CHECK( glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, texcoords.data()) );
}

void Sphere::draw(GLenum type)
{
    BaseElement::draw(type);
}
