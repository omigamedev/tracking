#pragma once

#include <map>
#include <string>

#define TEX_DIFFUSE     GL_TEXTURE0
#define TEX_NORMAL      GL_TEXTURE1
#define TEX_SPECULAR    GL_TEXTURE2
#define TEX_SHADOW      GL_TEXTURE3

#define ATT_VIGNETTE 1
#define ATT_TEXR     2
#define ATT_TEXG     3
#define ATT_TEXB     4

#define GLS_VER "#version 120\n"

class Shader
{
    GLuint m_prog;
    GLuint m_vert;
    GLuint m_frag;
    bool m_loaded;
    std::map<std::string, GLuint> m_locations;
    
    GLuint _loadShader(const std::string &source, GLenum type);
    GLuint _link();
    void _destroy();
    bool _checkCompilation(GLuint shader);
    bool _checkLinking(GLuint prog);
    bool _checkValidation(GLuint prog);
    
public:
    Shader();
    bool load(const std::string &vertex, const std::string &fragment);
    void unload();
    void use();
    bool isLoaded();
    GLuint getUniformLocation(const std::string &name);
    bool operator== (const Shader &b) const  { return m_prog == b.m_prog; }
};
