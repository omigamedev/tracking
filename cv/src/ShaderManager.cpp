#include "stdafx.h"
#include "ShaderManager.h"
#include <fstream>

ShaderManager::ShaderManager()
{
}

bool ShaderManager::loadShader(const std::string &name)
{
    Shader &shader = m_shaders[name];
    DBG("Loading shader: %s", name.c_str());
    char *vs = readFile("Shaders/" + name + "-vs.glsl");
    char *fs = readFile("Shaders/" + name + "-fs.glsl");
    if(shader.isLoaded())
        shader.unload();
    bool ret = shader.load(vs, fs);
    if(!ret) 
    {
        DBG("Error loading shader %s\n", name.c_str());
    }
    delete vs;
    delete fs;
    return ret;
}

bool ShaderManager::addShader(const std::string name, const std::string vertex, const std::string fragment)
{
    if (m_shaders.find(name) != m_shaders.end())
    {
        DBG("** Cached shader: %s", name.c_str());
        return true;
    }
    Shader &shader = m_shaders[name];
    DBG("** Loading shader: %s", name.c_str());
    const char *vs = vertex.c_str();
    const char *fs = fragment.c_str();
    if (shader.isLoaded())
        shader.unload();
    bool ret = shader.load(vs, fs);
    if (!ret)
    {
        DBG("Error loading shader %s\n", name.c_str());
    }
    else
    {
        shader.use();
        m_currentShader = &shader;
    }
    return ret;
}

void ShaderManager::useShader(const std::string &name)
{
    m_currentShader = &m_shaders[name];
    m_currentShader->use();
}

void ShaderManager::uniformMatrix4f(const std::string &name, const glm::mat4 &mat)
{
    GL_CHECK( glUniformMatrix4fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]) );
}

void ShaderManager::uniformMatrix4fv(const std::string &name, const float *mats, int count)
{
    GL_CHECK( glUniformMatrix4fv(m_currentShader->getUniformLocation(name), count, GL_FALSE, mats) );
}

void ShaderManager::uniformMatrix3f(const std::string &name, const glm::mat3 &mat)
{
    GL_CHECK( glUniformMatrix3fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]) );
}

void ShaderManager::uniform1i(const std::string &name, GLuint n)
{
    GL_CHECK( glUniform1i(m_currentShader->getUniformLocation(name), n) );
}

void ShaderManager::uniform2i(const std::string &name, const glm::ivec2 &v)
{
    GL_CHECK( glUniform2i(m_currentShader->getUniformLocation(name), v.x, v.y) );
}

void ShaderManager::uniform3i(const std::string &name, const glm::ivec3 &v)
{
    GL_CHECK( glUniform3i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z) );
}

void ShaderManager::uniform4i(const std::string &name, const glm::ivec4 &v)
{
    GL_CHECK( glUniform4i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w) );
}

void ShaderManager::uniform1f(const std::string &name, GLfloat f)
{
    GL_CHECK( glUniform1f(m_currentShader->getUniformLocation(name), f) );
}

void ShaderManager::uniform2f(const std::string &name, const glm::vec2 &v)
{
    GL_CHECK( glUniform2f(m_currentShader->getUniformLocation(name), v.x, v.y) );
}

void ShaderManager::uniform3f(const std::string &name, const glm::vec3 &v)
{
    GL_CHECK( glUniform3f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z) );
}

void ShaderManager::uniform4f(const std::string &name, const glm::vec4 &v)
{
    GL_CHECK( glUniform4f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w) );
}

char* ShaderManager::readFile(const std::string &filename, long *length)
{
    std::ifstream f(filename, std::ios_base::in | std::ios_base::ate);
    long len = (long)f.tellg();
    f.seekg(0, std::ios_base::beg);
    f.clear();
    char* buffer = new char[len+1];
    memset(buffer, 0, len+1);
    f.read(buffer, len);
    f.close();
    if(length) *length = len;
    return buffer;
}
