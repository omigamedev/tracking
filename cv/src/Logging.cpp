#include "stdafx.h"
#include "Logging.h"

char g_log_str[2048];

void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR)
    {
        DBG("OpenGL error %08x (%s), at %s:%i - for %s\n", err, gluErrorString(err), fname, line, stmt);
        __debugbreak();
    }
}

void PrintWinInfo()
{
    static const char reg[] = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion";
    static char exe[256];
    static char str[256];

    HKEY hKey;
    LONG res;

    res = RegOpenKeyExA(HKEY_LOCAL_MACHINE, reg, 0, KEY_READ, &hKey);
    if (res == ERROR_SUCCESS)
    {
        static char buffer[256];
        DWORD len;

        len = sizeof(buffer);
        RegQueryValueExA(hKey, "ProductName", 0, 0, (BYTE*)buffer, &len);

        DBG("OS: %s", buffer);
        len = sizeof(buffer);

        RegQueryValueExA(hKey, "EditionID", 0, 0, (BYTE*)buffer, &len);
        DBG("OS: %s", buffer);

        len = sizeof(buffer);
        RegQueryValueExA(hKey, "InstallationType", 0, 0, (BYTE*)buffer, &len);
        DBG("OS: %s", buffer);
    }

    RegCloseKey(hKey);
}

void PrintOpenGLInfo()
{
    const unsigned char* s = nullptr;
    GLint n = 0;
    DBG("GL version: %s", glGetString(GL_VERSION));
    DBG("GL vendor: %s", glGetString(GL_VENDOR));
    DBG("GL renderer: %s", glGetString(GL_RENDERER));

    int maxTextureSize;
    GL_CHECK( glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize) );
    DBG("GL max texres: %d", maxTextureSize);

    DBG("GLSL version: '%s'", glGetString(GL_SHADING_LANGUAGE_VERSION));
    GL_CHECK(glGetIntegerv(GL_NUM_EXTENSIONS, &n));
    for (int i = 0; i < n; i++)
    {
        GL_CHECK(s = glGetStringi(GL_EXTENSIONS, i));
        DBG("GL ext %03d: %s", i, s);
    }
    //GL_CHECK(glGetIntegerv(GL_NUM_SHADING_LANGUAGE_VERSIONS, &n));
    //for (int i = 0; i < n; i++)
    //{
    //    GL_CHECK(s = glGetStringi(GL_SHADING_LANGUAGE_VERSION, i));
    //    DBG("GLSL supported %02d: version %s", i, s);
    //}
}
