#pragma once

const char* g_shader_ColorFlat[] = {
    GLS_VER"uniform mat4 proj;uniform mat4 modelview;void main(){gl_Position = proj * modelview * gl_Vertex;}",
    GLS_VER"uniform vec4 color; void main(){ gl_FragColor = color; }"
};
const char* g_shader_UVCoord[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
void main()
{
    gl_FragColor = vec4(gl_TexCoord[0].st, 1, 1);
}
)!"
};
const char* g_shader_Checkerboard[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
void main()
{
    const vec4 c1 = vec4(1.0, 1.0, 1.0, 1.0);
    const vec4 c2 = vec4(0.9, 0.9, 0.9, 1.0);
    vec2 c = floor(fract(gl_TexCoord[0].st * 50.0) * 2.0);
    float alpha = mix(c.x, 1.0 - c.x, c.y);
    gl_FragColor = mix(c1, c2, alpha);
}
)!"
};
const char* g_shader_TextureFlat[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
void main()
{
    gl_FragColor = texture2D(tex0, gl_TexCoord[0].st);
}
)!"
};
const char* g_shader_TextureLayer[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
uniform float alpha;
void main()
{
    gl_FragColor = texture2D(tex0, gl_TexCoord[0].st) * vec4(1.0,1.0,1.0,alpha);
}
)!"
};
const char* g_shader_TextureBrush[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0; // brush
uniform sampler2D tex1; // canvas
uniform sampler2D tex2; // canvas t-1
uniform float alpha;
uniform float mixer;
uniform vec3 color;
void main()
{
    vec2 uv1 =  gl_TexCoord[0].st / gl_TexCoord[0].q;
    vec2 uv2 = gl_FragCoord.st / vec2(1024.0, 1024.0);
    float brush = ( 1.0 - texture2D(tex0, uv1).r ) * alpha;
    vec4 c2 = texture2D(tex1, uv2);
    vec4 c3 = texture2D(tex2, uv2);
    vec3 rgb = mix( c2.rgb, mix(color, c3.rgb, mixer), clamp( brush/(brush + c2.a), 0.0, 1.0 ) );
    float a = c2.a + (1.0 - c2.a) * brush;

    gl_FragColor = vec4(rgb, a);
}
)!"
};
const char* g_shader_CursorBrush[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform vec2 invres;
uniform float alpha;
uniform vec3 color;
void main()
{
    vec2 uv =  gl_TexCoord[0].st / gl_TexCoord[0].q;
    int has1 = 0;
    int has2 = 0;
    for (int x = -1; x < 2; x++)
    {
        for (int y = -1; y < 2; y++)
        {
            if (texture2D(tex0, uv + vec2(x, y) * invres).x == 1.0) 
                has1 = 1; 
            else 
                has2 = 2;
        }
    }
    vec4 brush = vec4(color, ( 1.0 - texture2D(tex0, uv).x ) * alpha);
    vec4 edge = vec4(fract(brush.rgb + 0.3), 0.6);
    gl_FragColor = mix(brush, edge, float(has1 + has2 - 2));
}
)!"
};
const char* g_shader_EraseBrush[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float alpha;
void main()
{
    vec2 uv1 =  gl_TexCoord[0].st / gl_TexCoord[0].q;
    vec2 uv2 = gl_FragCoord.st / vec2(1024.0, 1024.0);
    float brush = ( 1.0 - texture2D(tex0, uv1).r ) * alpha;
    vec4 c2 = texture2D(tex1, uv2);
    vec3 rgb = c2.rgb;
    float a = c2.a * (1.0 - brush);

    gl_FragColor = vec4(rgb, a);
}
)!"
};
const char* g_shader_TexturePano[] = {
// Vertex Shader 
R"!( #version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;
void main()
{
    gl_Position = proj * modelview * gl_Vertex;
    gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
)!",
// Fragment Shader 
R"!( #version 120
#define PI              3.1415926535897932384626433832795
#define TWO_PI          6.283185307179586476925286766559
#define ONE_ON_PI       0.31830988618379067153776752674503
#define ONE_ON_TWO_PI   0.15915494309189533576888376337251
uniform sampler2D tex0;
uniform mat3 rotation;
// https://en.wikipedia.org/wiki/Spherical_coordinate_system
// lon = u = phi   [0, 2P]
// lat = v = theta [0,  P]
void main()
{
    // [0, 1] -> lat-long
    float lat = gl_TexCoord[0].y * PI;
    float lon = gl_TexCoord[0].x * TWO_PI;
    // lat-long -> vec3
    vec3 p = vec3(sin(lat)*cos(lon), sin(lat)*sin(lon), cos(lat));
    // Rotate the sphere
    p = rotation * p;
    // [vec3] -> lat-long -> [0, 1]
    lat = acos(p.z) * ONE_ON_PI;
    lon = atan(p.y, p.x) * ONE_ON_TWO_PI;
    gl_FragColor = texture2D(tex0, vec2(lon, lat));
}
)!"
};

