#pragma once

void CheckOpenGLError(const char* stmt, const char* fname, int line);


void PrintWinInfo();
void PrintOpenGLInfo();

extern HWND g_hWnd;
#define DBG(M,...) { extern char g_log_str[2048]; int l=sprintf_s(g_log_str, "DBG: "##M##"\n", ##__VA_ARGS__); printf(g_log_str); }
#define ERR(M,...) { extern char g_log_str[2048]; int l=sprintf_s(g_log_str, "ERR: "##M##"\n", ##__VA_ARGS__); MessageBoxA(g_hWnd, g_log_str, "PanoPainter - Error", MB_OK | MB_ICONERROR); printf(g_log_str); }
#define DIE(M,...) { extern char g_log_str[2048]; int l=sprintf_s(g_log_str, "DIE: "##M##"\n", ##__VA_ARGS__); MessageBoxA(g_hWnd, g_log_str, "PanoPainter - Error", MB_OK | MB_ICONERROR); printf(g_log_str); exit(1); }

#ifdef _DEBUG
#define GL_CHECK(stmt) stmt; CheckOpenGLError(#stmt, __FILE__, __LINE__);
#else
#define GL_CHECK(stmt) stmt
#endif

