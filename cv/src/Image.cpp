#include "stdafx.h"
#include "Image.h"

bool Image::open(const char* filename, int components)
{
    if (data)
    {
        delete data;
        data = nullptr;
    }

    DBG("Image::open %s ...", filename);

    FILE *f;
    if (fopen_s(&f, filename, "rb") != 0 || f == 0)
    {
        static char str_err[1024];
        strerror_s(str_err, errno);
        DBG("Cannot open the file because: %s", str_err);
        return false;
    }

    stbi_set_flip_vertically_on_load(true);
    data = stbi_load_from_file(f, &w, &h, &comp, components);
    fclose(f);

    if (data == nullptr)
    {
        DBG("Image load failed: %s", stbi_failure_reason());
        return false;
    }
    DBG("Image loaded succesfully %dx%d %d components", w, h, comp);
    comp = components;

    return true;
}

void Image::destroy()
{
    if (data)
    {
        delete data;
        data = nullptr;
    }
    w = h = comp = 0;
}
