#pragma once

class Image
{
public:
    int w{ 0 };
    int h{ 0 };
    int comp{ 0 };
    uint8_t* data{ nullptr };
    Image() : data(nullptr), w(0), h(0), comp(0) {}
    bool open(const char* filename, int components);
    void destroy();
};

