#pragma once

#include "Shader.h"
#include <map>
#include <string>
#include <fstream>

class ShaderManager
{
public:
    std::map<std::string, Shader> m_shaders;
    std::map<std::string, GLuint> m_textures;
    Shader* m_currentShader;

    ShaderManager();
    
    virtual ~ShaderManager() { };
    
    bool createDepthBuffer(const glm::ivec2 &size, GLuint *outTexture, GLuint *outFBO);
    bool createColorBuffer(const glm::ivec2 &size, GLuint *outTexture, GLuint *outFBO);
    
    bool loadShader(const std::string &name);
    bool addShader(const std::string name, const std::string vertex, const std::string fragment);
    void useShader(const std::string &name);
    void uniformMatrix4f(const std::string &name, const glm::mat4 &mat);
    void uniformMatrix4fv(const std::string &name, const float *mats, int count);
    void uniformMatrix3f(const std::string &name, const glm::mat3 &mat);
    void uniform1i(const std::string &name, GLuint n);
    void uniform2i(const std::string &name, const glm::ivec2 &v);
    void uniform3i(const std::string &name, const glm::ivec3 &v);
    void uniform4i(const std::string &name, const glm::ivec4 &v);
    void uniform1f(const std::string &name, GLfloat f);
    void uniform2f(const std::string &name, const glm::vec2 &v);
    void uniform3f(const std::string &name, const glm::vec3 &v);
    void uniform4f(const std::string &name, const glm::vec4 &v);
    
    char* readFile(const std::string &filename, long *length = 0);
};
