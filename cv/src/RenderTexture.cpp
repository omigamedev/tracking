#include "stdafx.h"
#include "RenderTexture.h"

RenderTexture::RenderTexture()
{
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

RenderTexture::~RenderTexture()
{
    //destroy();
}

void RenderTexture::destroy()
{
    if (rboID)
    {
        GL_CHECK( glDeleteRenderbuffers(1, &rboID) );
    }
    if (texID)
    {
        unbindTexture();
        GL_CHECK( glDeleteTextures(1, &texID) );
    }
    if (fboID)
    {
        unbindFramebuffer();
        GL_CHECK( glDeleteFramebuffers(1, &fboID) );
    }
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

bool RenderTexture::create(int width, int height)
{
    // Destroy any previously created object
    destroy();

    w = width;
    h = height;

    GL_CHECK( glGenTextures(1, &texID) );
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, texID) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
    GL_CHECK( glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0) );
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, 0) );

    // Create a renderbuffer object to store depth info
    GL_CHECK( glGenRenderbuffers(1, &rboID) );
    GL_CHECK( glBindRenderbuffer(GL_RENDERBUFFER, rboID) );
    GL_CHECK( glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height) );
    GL_CHECK( glBindRenderbuffer(GL_RENDERBUFFER, 0) );

    // Create a framebuffer object
    GL_CHECK( glGenFramebuffers(1, &fboID) );
    GL_CHECK( glBindFramebuffer(GL_FRAMEBUFFER, fboID) );

    // Attach the texture to FBO color attachment point
    GL_CHECK( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texID, 0) );

    // Attach the renderbuffer to depth attachment point
    GL_CHECK( glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboID) );

    // Check FBO status
    GL_CHECK( GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) );
    if (status != GL_FRAMEBUFFER_COMPLETE)
        ERR("createColorBuffer failed");

    // Switch back to window-system-provided framebuffer
    GL_CHECK( glBindFramebuffer(GL_FRAMEBUFFER, 0) );

    return status == GL_FRAMEBUFFER_COMPLETE;
}

void RenderTexture::bindFramebuffer()
{
    GL_CHECK( glBindFramebuffer(GL_FRAMEBUFFER, fboID) );
}

void RenderTexture::unbindFramebuffer()
{
    GL_CHECK( glBindFramebuffer(GL_FRAMEBUFFER, 0) );
}

void RenderTexture::clear(glm::vec4 color)
{
    GL_CHECK( glClearColor(color.r, color.g, color.b, color.a) );
    GL_CHECK( glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) );
}

void RenderTexture::readTextureData(uint8_t* buffer)
{
    bindTexture();
    GL_CHECK( glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer) );
    unbindTexture();
}

uint8_t* RenderTexture::createBuffer()
{
    return new uint8_t[w * h * 3];
}

void RenderTexture::bindTexture()
{
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, texID) );
}

void RenderTexture::unbindTexture()
{
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, 0) );
}
