#include "stdafx.h"
#include "Shader.h"

Shader::Shader()
{
    m_prog = 0;
    m_vert = 0;
    m_frag = 0;
    m_loaded = false;
}

GLuint Shader::_loadShader(const std::string &source, GLenum type)
{
    GL_CHECK( GLuint shader = glCreateShader(type) );
    const char *str = source.c_str();
    GL_CHECK( glShaderSource(shader, 1, &str, 0) );
    GL_CHECK( glCompileShader(shader) );
    return _checkCompilation(shader) ? shader : 0;
}

GLuint Shader::_link()
{
    GLuint prog = glCreateProgram();
    GL_CHECK( glAttachShader(prog, m_vert) );
    GL_CHECK( glAttachShader(prog, m_frag) );
    GL_CHECK( glLinkProgram(prog) );
    if(!_checkLinking(prog)) return 0;
    
//    glValidateProgram(prog);
//    if(!_checkValidation(prog)) return 0;
    
    return prog;
}

void Shader::_destroy()
{
    GL_CHECK( glDetachShader(m_prog, m_vert) );
    GL_CHECK( glDetachShader(m_prog, m_frag) );
    GL_CHECK( glDeleteProgram(m_prog) );
    m_locations.clear();
    m_loaded = false;
}

bool Shader::_checkCompilation(GLuint shader)
{
    static GLchar msg[1024];
    GLint r;
    GL_CHECK( glGetShaderiv(shader, GL_COMPILE_STATUS, &r) );
    if(!r)
    {
        GL_CHECK( glGetShaderInfoLog(shader, sizeof(msg), 0, msg) );
        DBG("Compiling shader failed:\n%s", msg);
        return 0;
    }
    else
    {
        GL_CHECK(glGetShaderInfoLog(shader, sizeof(msg), 0, msg));
        if (strlen(msg))
            DBG("Compiling shader warnings:\n%s", msg);
    }
    return 1;
}

bool Shader::_checkLinking(GLuint prog)
{
    static GLchar msg[1024];
    GLint r;
    GL_CHECK( glGetProgramiv(prog, GL_LINK_STATUS, &r) );
    if(!r)
    {
        GL_CHECK( glGetProgramInfoLog(prog, sizeof(msg), 0, msg) );
        DBG("Linking shaders failed: %s", msg);
        return 0;
    }
    else
    {
        GL_CHECK(glGetProgramInfoLog(prog, sizeof(msg), 0, msg));
        if (strlen(msg))
            DBG("Linking shaders warnings: %s", msg);
    }
    return 1;
}

bool Shader::_checkValidation(GLuint prog)
{
    GLint r;
    GL_CHECK( glGetProgramiv(prog, GL_VALIDATE_STATUS, &r) );
    if(!r)
    {
        static GLchar msg[1024];
        GL_CHECK( glGetProgramInfoLog(prog, sizeof(msg), 0, msg) );
        DBG("%s\n", msg);
        return 0;
    }
    return 1;
}

bool Shader::load(const std::string &vertex, const std::string &fragment)
{
    m_loaded = false;
    if (!(m_vert = _loadShader(vertex, GL_VERTEX_SHADER))) { DBG("Vertex failed"); return 0; };
    if (!(m_frag = _loadShader(fragment, GL_FRAGMENT_SHADER))) { DBG("Fragment failed"); return 0; };
    if (!(m_prog = _link())) { _destroy(); DBG("Linking failed"); return 0; }
    m_loaded = 1;
    return 1;
}

void Shader::unload()
{
    _destroy();
}

void Shader::use()
{
    GL_CHECK( glUseProgram(m_prog) );
}

bool Shader::isLoaded()
{
    return m_loaded;
}

GLuint Shader::getUniformLocation(const std::string &name)
{
    if(m_locations.count(name) == 0)
        GL_CHECK( m_locations[name] = glGetUniformLocation(m_prog, name.c_str()) );
    return m_locations[name];
}
