#include "stdafx.h"
#include "Primitive.h"
#include "ShaderManager.h"
#include "Texture.h"
#include "ShadersCode.h"

HWND g_hWnd;

struct Camera
{
    glm::mat4 proj; // opengl projection matrix
    cv::Matx33f cvmat; // cv intrinsic matrix
    void create(int w, int h, float fx, float fy)
    {
        float p_near = .1f;
        float p_far = 10.f;
        float X = p_near + p_far;
        float Y = p_near * p_far;
        float x0 = w/2;
        float y0 = h/2;
        float s = 0; // shear
        proj = glm::ortho<float>(0, w, 0, h, p_near, p_far);
        glm::mat4 cam_intrinsic = glm::mat4(
            fx,  s, -x0, 0,
             0, fy, -y0, 0,
             0,  0,   1, 0,
             0,  0,  -1, 0
            );
        cvmat = cv::Matx33f(
            fx,  s,  x0,
             0, fy,  y0,
             0,  0,   1
        );
        proj = proj * glm::transpose(cam_intrinsic);
    }
};

void printm(const char* name, const glm::mat4& m)
{
    printf("%s:\n", name);
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            printf("%6.2f ", m[i][j]);
        }
        printf("\n");
    }
}

void printm(const char* name, const cv::Mat& m)
{
    printf("%s:\n", name);
    for (int i = 0; i < m.rows; i++)
    {
        for (int j = 0; j < m.cols; j++)
        {
            printf("%6.2f ", m.at<double>(i, j));
        }
        printf("\n");
    }
}

void update_matrices()
{

}

int main()
{
    glfwInit();
    GLFWwindow* window = glfwCreateWindow(512, 512, "OpenCV PnP Solver", nullptr, nullptr);
    g_hWnd = glfwGetWin32Window(window);
    glfwMakeContextCurrent(window);
    if (glewInit() != GLEW_OK)
        DIE("Error in glewInit");

    GL_CHECK( glEnable(GL_TEXTURE_2D) );
    GL_CHECK( glDisable(GL_DEPTH_TEST) );
    GL_CHECK( glEnableClientState(GL_VERTEX_ARRAY) );
    GL_CHECK( glEnableClientState(GL_TEXTURE_COORD_ARRAY) );

    GL_CHECK( glActiveTexture(GL_TEXTURE0) );

    Camera cam;
    cam.create(512, 512, 300, 300);

    Cube cube;
    cube.create(1, .5, 2);

    ShaderManager sm;
    sm.addShader("color-flat", g_shader_ColorFlat[0], g_shader_ColorFlat[1]);
    sm.uniformMatrix4f("proj", cam.proj);

//     glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//     glLineWidth(3);
    glPointSize(4);

    float theta = 0;
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        theta += glm::radians(1.f);
        glm::mat4 mv = glm::lookAt(glm::vec3(sinf(theta) * 10, 5, -5), glm::vec3(0), glm::vec3(0, 1, 0));

        std::vector<cv::Point3f> objectPoints;
        std::vector<cv::Point2f> imagePoints;

        glm::vec3 points[7];
        for (int i = 0; i < 7; i++)
        {
            points[i] = glm::project(cube.points[i], mv, cam.proj, glm::vec4(0, 0, 512, 512));
            objectPoints.push_back(cv::Point3f(cube.points[i].x, cube.points[i].y, -cube.points[i].z));
            imagePoints.push_back(cv::Point2f(points[i].x, 512-points[i].y));
        }


        glm::mat4 newmv;
        cv::Mat rvec, tvec;
        if (cv::solvePnP(objectPoints, imagePoints, cam.cvmat, cv::Mat(), rvec, tvec))
        {
            cv::Mat R;
            cv::Rodrigues(rvec, R); // R is 3x3

            R = R.t();  // rotation of inverse

            printm("T", tvec);
            printm("R", R);

            // copy rotation
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    newmv[i][j] = R.at<double>(i, j);

            // copy translation
            newmv[3][0] = tvec.at<double>(0, 0);
            newmv[3][1] = tvec.at<double>(1, 0);
            newmv[3][2] = tvec.at<double>(2, 0);

            // change sign for some reason
            newmv[0][0] *= -1;
            newmv[2][1] *= -1;
            newmv[2][2] *= -1;
        }

        printm("camera", mv);
        printm("PnP camera", newmv);
        printf("---------------\n");

        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        glViewport(0, 0, 512, 512);

        // Draw the cube
        //sm.uniform4f("color", { 1, 0, 0, 1 });
        //sm.uniformMatrix4f("modelview", mv);
        //cube.draw(GL_POINTS);

        // Draw the cube using the PnP camera
        sm.uniform4f("color", { 1, 0, 0, 1 });
        sm.uniformMatrix4f("modelview", newmv);
        cube.draw(GL_POINTS);

        // Draw the 2D points directly on the framebuffer
        sm.uniform4f("color", { 0, 1, 0, 1 });
        glDrawBuffer(GL_BACK);
        for (auto p : points)
        {
            static glm::vec3 blue[16];
            glWindowPos2f(p.x, p.y);
            glDrawPixels(4, 4, GL_RGB, GL_FLOAT, &blue);
        }

        glfwSwapBuffers(window);
    }

    return 0;
}

