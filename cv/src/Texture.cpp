#include "stdafx.h"
#include "Texture.h"

static GLenum color_map[] = { GL_R, 0, GL_RGB, GL_RGBA };
static GLenum color_map_f[] = { GL_R32F, 0, GL_RGB32F, GL_RGBA32F };

bool Texture::open(const char* filename, int componets)
{
    if (tex > 0)
    {
        DBG("Destroy old texture");
        GL_CHECK(glDeleteTextures(1, &tex));
        tex = 0;
    }

    if (data)
    {
        delete data;
        data = nullptr;
    }

    DBG("Loading image %s ...", filename);

    FILE *f;
    if (fopen_s(&f, filename, "rb") != 0 || f == 0)
    {
        static char str_err[1024];
        strerror_s(str_err, errno);
        ERR("Error in file %s\nCannot open the file because: %s", filename, str_err);
        return false;
    }

    stbi_set_flip_vertically_on_load(true);
    data = stbi_load_from_file(f, &w, &h, &comp, componets);
    fclose(f);

    comp = componets;

    if (data == nullptr)
    {
        ERR("Image load failed: %s", stbi_failure_reason());
        return false;
    }
    DBG("Image loaded succesfully %dx%d %d components", w, h, comp);

    int maxTextureSize;
    GL_CHECK( glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize) );
    if (w > maxTextureSize || h > maxTextureSize)
    {
        ERR("The image resolution %dx%d is too large. Max supported is %dx%d", w, h, maxTextureSize, maxTextureSize);
        delete data;
        data = nullptr;
        return false;
    }

    GLenum fmt = color_map[comp - 1];

    GL_CHECK( glGenTextures(1, &tex) );
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, tex) );
    GL_CHECK( glTexImage2D(GL_TEXTURE_2D, 0, fmt, w, h, 0, fmt, GL_UNSIGNED_BYTE, data) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK( glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, 0) );
    DBG("Texture params set");
    
    return true;
}

void Texture::set(unsigned char* data, int width, int height, int components, bool is_float/* = false*/)
{
    if (tex > 0)
    {
        DBG("Destroy old texture");
        GL_CHECK(glDeleteTextures(1, &tex));
        tex = 0;
    }

    comp = components;
    w = width;
    h = height;

    int maxTextureSize;
    GL_CHECK(glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize));
    if (w > maxTextureSize || h > maxTextureSize)
    {
        ERR("The image resolution %dx%d is too large. Max supported is %dx%d", w, h, maxTextureSize, maxTextureSize);
    }

    GLenum fmt_internal = is_float ? color_map_f[comp - 1] : color_map[comp - 1];
    GLenum fmt = color_map[comp - 1];

    GL_CHECK(glGenTextures(1, &tex));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, tex));
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, fmt_internal, w, h, 0, fmt, GL_UNSIGNED_BYTE, data));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, 0));
    DBG("Texture params set");
}

void Texture::destroy()
{
    if (tex > 0)
    {
        DBG("Destroy old texture");
        GL_CHECK(glDeleteTextures(1, &tex));
        tex = 0;
    }

    if (data)
    {
        delete data;
        data = nullptr;
    }
}

void Texture::update()
{
    GLenum fmt = color_map[comp - 1];
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, tex) );
    GL_CHECK( glTexImage2D(GL_TEXTURE_2D, 0, fmt, w, h, 0, fmt, GL_UNSIGNED_BYTE, data) );
    GL_CHECK( glBindTexture(GL_TEXTURE_2D, 0) );
}
