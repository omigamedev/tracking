#pragma once

class RenderTexture
{
    GLuint fboID;
    GLuint rboID;
    GLuint texID;
    int w;
    int h;

public:
    RenderTexture();
    ~RenderTexture();

    void destroy();
    bool create(int width, int height);
    void clear(glm::vec4 color = glm::vec4(0));
    void readTextureData(uint8_t* buffer);
    uint8_t* createBuffer();
    void bindFramebuffer();
    void unbindFramebuffer();
    void bindTexture();
    void unbindTexture();
    GLuint getTextureID() { return texID; }
    int getWidth() { return w; }
    int getHeight() { return h; }
};

