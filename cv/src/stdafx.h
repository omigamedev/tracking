#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES

#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#define GLFW_INCLUDE_GLU

#define GLEW_STATIC

#define GLM_FORCE_SWIZZLE 
#define GLM_FORCE_RADIANS

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <gl\glew.h>
#include <glfw\glfw3.h>
#include <glfw\glfw3native.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\projection.hpp>
#include <opencv2\opencv.hpp>
#include <stb_image.h>
#include <stb_image_write.h>

#include "Logging.h"
