#pragma once

class Texture
{
public:
    GLuint tex = 0;
    int w = 0;
    int h = 0;
    int comp = 0;
    unsigned char* data = nullptr;

    bool open(const char* filename, int componets = 3);
    void set(unsigned char* data, int width, int height, int components, bool is_float = false);
    void destroy();
    void update();
};
