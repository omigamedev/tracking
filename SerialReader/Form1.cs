﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialReader
{
    delegate void UpdateTextCallback();

    struct Sensor
    {
        public int x;
        public int y;
    }

    public partial class Form1 : Form
    {
        private string data = "";
        Sensor[] sensors = new Sensor[2];

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            serialPort1.Open();
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            int len = serialPort1.BytesToRead;
            byte[] buffer = new byte[len];
            serialPort1.Read(buffer, 0, len);
            string s = Encoding.UTF8.GetString(buffer);
            data += s;
            UpdateText();
        }

        private void UpdateText()
        {
            if (InvokeRequired)
            {
                Invoke(new UpdateTextCallback(UpdateText));
            }
            else
            {
                int start = data.IndexOf('(');
                if (start == -1) return;
                int end = data.IndexOf(')', start);
                if (start != -1 && end != -1)
                {
                    var row = data.Substring(start+1, end - start - 2);
                    data = data.Substring(end + 1);
                    if (!checkBox1.Checked)
                        return;
                    var pair = row.Split(new char[] { ';' });
                    if (pair.Length < 2)
                        return;
                    for (int i = 0; i < 2; i++)
                    {
                        var coords = pair[i].Split(new char[] { ' ' });
                        if (coords.Length == 2)
                        {
                            int.TryParse(coords[0], out sensors[i].x);
                            int.TryParse(coords[1], out sensors[i].y);
                        }
                    }
                    textBox1.AppendText(string.Format("{0,4}  {1,3}\n", sensors[0].x, sensors[0].y));
                    textBox2.AppendText(string.Format("{0,4}  {1,3}\n", sensors[1].x, sensors[1].y));
                }
                pictureBox1.Invalidate();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort1.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Black);
            foreach (var s in sensors)
            {
                int x = (int)(s.x / 8000.0 * pictureBox1.Width);
                int y = (int)(s.y / 8000.0 * pictureBox1.Height);
                e.Graphics.FillRectangle(new SolidBrush(Color.White), x, y, 2, 2);
            }
        }
    }
}
